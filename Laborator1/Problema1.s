.data
    x: .long 15
    y: .long 12
.text

.global main

main:
    MOV x, %EAX
    MOV y, %EBX
    MOV %EBX, x
    MOV %EAX, y   

et_exit:
    MOV $1, %EAX
    MOV $0, %EBX
    INT $0x80