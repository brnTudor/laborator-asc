.data
    x: .space 12

.text

.global main

main:
    MOV $3, %EAX
    MOV $0, %EBX
    MOV $x, %ECX
    MOV $12, %EDX
    INT $0x80

print:
    MOV $4, %EAX
    MOV $1, %EBX
    MOV $x, %ECX
    MOV $12, %EDX
    INT $0x80

et_exit:
    MOV $1, %EAX
    MOV $0, %EBX
    INT $0x80